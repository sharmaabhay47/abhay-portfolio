import codeBackground from "assest/code.jpg"
import mobileIcon from "assest/mobile.png"
import computerIcon from "assest/computer.png"
import craftsmanIcon from "assest/craftsmanLogo.jpg"
import meIcon from "assest/me.jpg"
import bandhanIcon from "assest/bandhan.png"
import kiddoIcon from "assest/kiddo.png"
import heirshipIcon from "assest/heirship.png"
import squareIcon from "assest/square.png"
import gccIcon from "assest/gcc.png"
import jagoIcon from "assest/jago.webp"

const ImageConstant = {
  CODE_BACKGROUND: codeBackground,
  MOBILE_ICON: mobileIcon,
  COMPUTER_ICON: computerIcon,
  CRAFTSMAN_IMAGE: craftsmanIcon,
  BANDHAN_IMAGE: bandhanIcon,
  KIDDO_ICON: kiddoIcon,
  HEIRSHIP_ICON: heirshipIcon,
  ME_ICON: meIcon,
  SQUARE_ICON: squareIcon,
  GCC_ICON: gccIcon,
  JAGO_ICON: jagoIcon,
}

export default ImageConstant
