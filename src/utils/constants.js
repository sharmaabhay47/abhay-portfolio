const Constants = {
  containerElement: "containerElement",
  firstInsideContainer: "home",
  secondInsideContainer: "services",
  thirdInsideContainer: "portfolio",
  fourthInsideContainer: "about",
  fivthInsideContainer: "contact",
}

export default Constants
