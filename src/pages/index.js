import React from "react"
import loadable from "loadable-components"

import Home from "pages/home"
import Layout from "components/layout"
import SEO from "components/seo.js"
import Constants from "utils/constants"

class App extends React.Component {
  ReactScroll = loadable(() =>
    import("react-scroll/modules/components/Element")
  )

  constructor() {
    super()
    this.state = { height: 0 }
  }

  componentDidMount() {
    this.updateWindowDimensions()
    window.addEventListener("resize", this.updateWindowDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions)
  }

  updateWindowDimensions = () => this.setState({ height: window.innerHeight })

  render() {
    const ReactScrollElement = this.ReactScroll
    const { height } = this.state
    return (
      <div
        style={{
          width: "100%",
          position: "relative",
          height: height,
          overflow: "hidden",
        }}
      >
        <ReactScrollElement
          id={Constants.containerElement}
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            bottom: -15,
            right: -20,
            overflow: "scroll",
          }}
        >
          <Layout>
            <SEO />
            <Home />
          </Layout>
        </ReactScrollElement>
      </div>
    )
  }
}
export default App
