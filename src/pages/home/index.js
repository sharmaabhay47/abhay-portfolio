import React from "react"

import loadable from "loadable-components"

import MyServices from "components/services"
import Portfolios from "components/portfolios"
import About from "components/about"
import Footer from "components/footer"
import Header from "components/header"
import Contact from "components/contact"

import Constants from "utils/constants"

class Home extends React.Component {
  ReactScroll = loadable(() =>
    import("react-scroll/modules/components/Element")
  )
  render() {
    const ReactScrollElement = this.ReactScroll
    return (
      <>
        <Header />

        <ReactScrollElement name={Constants.secondInsideContainer}>
          <MyServices />
        </ReactScrollElement>
        <ReactScrollElement name={Constants.thirdInsideContainer}>
          <Portfolios />
        </ReactScrollElement>
        <ReactScrollElement name={Constants.fourthInsideContainer}>
          <About />
        </ReactScrollElement>
        <ReactScrollElement name={Constants.fivthInsideContainer}>
          <Contact />
        </ReactScrollElement>
        <Footer />
      </>
    )
  }
}

export default Home
