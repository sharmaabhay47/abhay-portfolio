import React from "react"

import SEO from "components/seo"
import Link from "components/link"

const NotFoundPage = () => (
  <div
    style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
      height: "-webkit-fill-available",
      backgroundColor: "black",
    }}
  >
    <SEO title="404: Not found" />
    <h1
      style={{
        color: "#dc143c",
      }}
    >
      PAGE NOT FOUND
    </h1>
    <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    <Link to="/" link="Go Home" />
  </div>
)

export default NotFoundPage
