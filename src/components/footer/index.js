import React from "react"
import { FiFacebook, FiGithub, FiChevronsUp } from "react-icons/fi"
import { GrLinkedin } from "react-icons/gr"
import { FaStackOverflow } from "react-icons/fa"
import { AiOutlineHeart } from "react-icons/ai"

import styles from "./footer.module.scss"

function Footer() {
  return (
    <div className={styles.footerContainer}>
      {renderScrollUp()}
      <div className={styles.footerIconContainer}>
        <IconCard
          link="https://www.facebook.com/abhay.sharma.1650/"
          icon={<FiFacebook />}
        />
        <IconCard
          link="https://www.linkedin.com/in/abhay-sharma-3955a73b/"
          icon={<GrLinkedin />}
        />
        <IconCard
          link="https://stackoverflow.com/users/9463179/abhay-sharma"
          icon={<FaStackOverflow />}
        />
        <IconCard link="https://github.com/sharmaabhay18" icon={<FiGithub />} />
      </div>
      {textContainer()}
    </div>
  )
}

const renderScrollUp = () => (
  <div className={styles.footerScrollUpContainer}>
    <FiChevronsUp
      onClick={() => {
        return (
          document &&
          document.querySelector("#containerElement").scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth",
          })
        )
      }}
      className={styles.scrollUpIconStyle}
    />
  </div>
)

const textContainer = () => (
  <div className={styles.footerTextContainer}>
    <h4 className={styles.footerTextStyle}>
      ABHAY SHARMA <p className={styles.footerCopyrightTextStyle}>©2020</p>
    </h4>
    <h4 className={styles.footerTextStyle}>
      All rights reserved | Made with
      <AiOutlineHeart
        style={{
          padding: "0 2px",
        }}
      />
      using Gatsby
    </h4>
    <h6
      className={styles.footerTextStyle}
      style={{
        marginBottom: "20px",
      }}
    >
      {" "}
      Landing Photo by{" "}
      <a
        href="https://unsplash.com/@markusspiske?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"
        style={{
          backgroundColor: "transparent",
          margin: 0,
          padding: "0 5px",
          fontSize: "12px",
          textDecoration: "underline",
          "&:hover": {
            textDecoration: "underline",
          },
        }}
      >
        Markus Spiske{" "}
      </a>{" "}
      on Unsplash
    </h6>
  </div>
)

function IconCard({ link, icon }) {
  return (
    <div className={styles.iconContainer}>
      <a className={styles.iconStyle} href={link}>
        {icon}
      </a>
    </div>
  )
}
export default Footer
