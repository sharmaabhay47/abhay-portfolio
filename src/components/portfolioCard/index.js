import React from "react"
import PropTypes from "prop-types"

import styles from "./portfolioCard.module.scss"

function PortfolioCard({ backgroundImage, text, children }) {
  return (
    <div className={styles.mainContainer}>
      <div className={styles.portfolioContainer}>
        <img
          src={backgroundImage}
          alt="backgroundImage"
          className={styles.portfolioImage}
        />
        <div className={styles.portfolioOverlay}>
          <div className={styles.portfolioContent}>
            <p>{text}</p>
            <div className={styles.buttonContainer}>{children}</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PortfolioCard

PortfolioCard.prototype = {
  backgroundImage: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}
