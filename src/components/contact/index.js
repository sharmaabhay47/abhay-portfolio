import React from "react"
import Loader from "react-loader-spinner"

import styles from "./contact.module.scss"

class Contact extends React.Component {
  constructor(props) {
    super(props)
    this.state = { halfWidth: 0, status: "", loading: false }
  }

  submitForm = ev => {
    this.setState({ loading: true })
    ev.preventDefault()
    const form = ev.target
    const data = new FormData(form)
    const xhr = new XMLHttpRequest()
    xhr.open(form.method, form.action)
    xhr.setRequestHeader("Accept", "application/json")
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return
      if (xhr.status === 200) {
        form.reset()
        this.setState({ status: "SUCCESS", loading: false })
      } else {
        this.setState({ status: "ERROR", loading: false })
      }
    }
    xhr.send(data)
  }

  componentDidMount() {
    this.updateWindowDimensions()
    window.addEventListener("resize", this.updateWindowDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions)
  }

  updateWindowDimensions = () =>
    this.setState({ halfWidth: window.innerWidth / 2 })

  render() {
    const { halfWidth, status, loading } = this.state
    return (
      <>
        <div className={styles.triangle}>
          <div
            className={styles.leftTriangle}
            style={{
              borderWidth: `200px 0 0 ${halfWidth}px`,
            }}
          />
          <div
            className={styles.rightTriangle}
            style={{
              borderWidth: `0 0 200px ${halfWidth}px`,
            }}
          />
        </div>
        <div className={styles.contactContainer}>
          <div className={styles.headerContactContainer}>
            <h1 className={styles.contactTitleStyle}>Contact</h1>
            <div className={styles.horizontalLineStyle} />
          </div>
          <h3 className={styles.contactSubTitleStyle}>
            Have a question or want to work together ?
          </h3>
          <form
            onSubmit={this.submitForm}
            action="https://formspree.io/xeqreyae"
            method="POST"
            className={styles.formContainer}
          >
            <input
              className={styles.inputFeildStyle}
              type="text"
              id="name"
              name="name"
              placeholder="Enter Name"
              required
              label="name"
            />
            <input
              className={styles.inputFeildStyle}
              type="email"
              id="email"
              name="email"
              placeholder="Enter Email"
              required
              label="email"
            />
            <textarea
              placeholder="Enter Your Message"
              type="text"
              name="message"
              label="message"
              className={styles.inputFeildStyle}
              style={{
                minHeight: "150px",
              }}
            />

            {status === "SUCCESS" ? (
              <>
                <h3 className={styles.successStyle}>
                  Thanks! Will get back soon to you.
                </h3>
                {loading ? (
                  <div
                    className={styles.loaderContainer}
                    style={{
                      margin: 0,
                    }}
                  >
                    <Loader
                      type="Bars"
                      color="#dc143c"
                      height={50}
                      width={50}
                    />
                  </div>
                ) : (
                  <input
                    className={styles.submitButtonStyle}
                    style={{
                      margin: "5px",
                    }}
                    type="submit"
                    value="Submit Another"
                  />
                )}
              </>
            ) : (
              <>
                {loading ? (
                  <div className={styles.loaderContainer}>
                    <Loader
                      type="Bars"
                      color="#dc143c"
                      height={50}
                      width={50}
                    />
                  </div>
                ) : (
                  <input
                    className={styles.submitButtonStyle}
                    type="submit"
                    value="Submit"
                  />
                )}
              </>
            )}
            {status === "ERROR" && (
              <p className={styles.errorStyle}>
                Ooops! There was an error. Please try again!
              </p>
            )}
          </form>
        </div>
      </>
    )
  }
}

export default Contact
