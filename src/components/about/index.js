import React from "react"

import imageConstants from "utils/imageConstant"
import resume from "assest/abhay-resume.pdf"
import styles from "./about.module.scss"

function About() {
  return (
    <>
      <div className={styles.aboutContainer}>
        <div className={styles.subContainer}>
          <div className={styles.aboutBackgroundStyle}>About</div>
          <img
            src={imageConstants.SQUARE_ICON}
            className={styles.squareImageStyle}
            alt="square"
          />
          <div className={styles.textContainer}>
            <h1>About Me</h1>
            <p className={styles.textContainerContent}>
              I am Abhay Sharma, a Software Engineer 👨‍💻 by profession. I am
              currently based in Pune, India you will find me mostly playing
              video games 🎮 during weekends, or get high on caffeine ☕, or
              trekking🚶‍♂️somewhere midst nowhere. My current stack includes
              Javascript, TypeScript, Dart, React.js, React Native, Flutter,
              Nodejs, GraphQL, Gatsby (Used for building this portfolio 😌) and
              all the other various frameworks, libraries and technologies
              related to them. And please, wubba lubba dub-dub
            </p>
            <DownloadButton />
          </div>
          <img
            src={imageConstants.ME_ICON}
            className={styles.aboutImageStyle}
            alt="about"
          />
        </div>
      </div>
    </>
  )
}

function DownloadButton() {
  return (
    <div className={styles.downloadButtonContainer}>
      <a href={resume} download className={styles.downloadButtonStyle}>
        Download CV
      </a>
    </div>
  )
}

export default About
