import React from "react"

import PortfolioCard from "components/portfolioCard"
import Button from "components/button"

import imageConstants from "utils/imageConstant"

import styles from "./portfolios.module.scss"

function Portfolios() {
  return (
    <>
      <div className={styles.portfolioContainer}>
        <div className={styles.portfolioTextContainer}>
          <p>PORTFOLIOS</p>
          <h1>Awesome Projects that I have worked on</h1>
        </div>
      </div>
      <div className={styles.portfolioCardContainer}>
        <PortfolioCard
          backgroundImage={imageConstants.JAGO_ICON}
          text="Jago is an app that makes money management simple, collaborative and innovative. Life Finance Solution that makes Life Simple in the Digital Age. "
        >
          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit PlayStore"
            link="https://play.google.com/store/apps/details?id=com.jago.digitalBanking"
          />

          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit AppStore"
            link="https://apps.apple.com/ID/app/id1539402234?mt=8"
          />
        </PortfolioCard>
        <PortfolioCard
          backgroundImage={imageConstants.GCC_ICON}
          text="Green Chick Chop is a Food delivery application where people can select nearby store based on their location and order food."
        >
          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit Website"
            link="https://greenchickchopindia.com/"
          />
        </PortfolioCard>

        <PortfolioCard
          backgroundImage={imageConstants.BANDHAN_IMAGE}
          text="	
          Bandhan is an organization which conduct camps over India and this application lets the team connect and help patients get the benefit of medicines."
        >
          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit PlayStore"
            link="https://play.google.com/store/apps/details?id=com.bandhan"
          />
        </PortfolioCard>
        <PortfolioCard
          backgroundImage={imageConstants.CRAFTSMAN_IMAGE}
          text="The Craftsman Republic App provides everything Craftsmen need to sign-up, receive, and manage pre-paid Workorders across the U.S."
        >
          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit PlayStore"
            link="https://play.google.com/store/apps/details?id=com.cr.craftsmanrepublic"
          />

          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit AppStore"
            link="https://apps.apple.com/us/app/craftsman-republic/id1104393495"
          />
        </PortfolioCard>
        <PortfolioCard
          backgroundImage={imageConstants.KIDDO_ICON}
          text="	
            Kiddo is a personalized smart health platform for children."
        >
          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit PlayStore"
            link="https://play.google.com/store/apps/details?id=com.kiddowear.kiddo_single"
          />

          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit AppStore"
            link="https://apps.apple.com/us/app/kiddowear/id1487276458"
          />
        </PortfolioCard>
        <PortfolioCard
          backgroundImage={imageConstants.HEIRSHIP_ICON}
          text="	
          Heirship is a photo aggregation tool which collects data from the users multiple cloud storage accounts and displays unique Insights and analytics like number of duplicate photos."
        >
          <Button
            buttonStyle={styles.buttonStyle}
            buttonText="Visit Website"
            link="https://heirship.io/"
          />
        </PortfolioCard>
      </div>
    </>
  )
}

export default Portfolios
