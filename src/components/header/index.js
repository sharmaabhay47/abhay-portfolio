import React, { useState, useRef, useEffect } from "react"
import PropTypes from "prop-types"
import loadable from "loadable-components"
import { GiHamburgerMenu } from "react-icons/gi"

import Constants from "utils/constants"
import styles from "./header.module.scss"

const ReactScrollLink = loadable(() =>
  import("react-scroll/modules/components/Link")
)

function Header() {
  const [isSticky, setSticky] = useState(false)
  const [isHidden, setHidden] = useState(false)
  const ref = useRef(null)

  useEffect(() => {
    document
      .querySelector("#containerElement")
      .addEventListener("scroll", handleScroll)

    return () => {
      document
        .querySelector("#containerElement")
        .removeEventListener("scroll", () => handleScroll)
    }
  }, [])

  const handleScroll = () => {
    if (ref && ref.current && ref.current.getBoundingClientRect()) {
      setSticky(ref.current.getBoundingClientRect().bottom <= 600)
    }
  }

  const showSettings = () => setHidden(!isHidden)

  return (
    <header ref={ref}>
      <div
        className={
          !isHidden
            ? isSticky
              ? styles.stickyHeader
              : styles.normalHeader
            : styles.expandHamburgerStyle
        }
      >
        <h3 className={styles.linkContainer}>
          <a
            className={styles.link}
            style={{
              transition: "0.5s ease",
            }}
            onClick={() => {
              isHidden && showSettings()
              return (
                document &&
                document.querySelector("#containerElement").scrollTo({
                  top: 0,
                  left: 0,
                  behavior: "smooth",
                })
              )
            }}
          >
            Home
          </a>
        </h3>
        <ScrollLink
          isHidden={isHidden}
          showSettings={showSettings}
          to={Constants.secondInsideContainer}
          text="Services"
        />
        <ScrollLink
          isHidden={isHidden}
          showSettings={showSettings}
          to={Constants.thirdInsideContainer}
          text="Portfolio"
        />
        <ScrollLink
          isHidden={isHidden}
          showSettings={showSettings}
          to={Constants.fourthInsideContainer}
          text="About"
        />
        <ScrollLink
          isHidden={isHidden}
          showSettings={showSettings}
          to={Constants.fivthInsideContainer}
          text="Contact"
        />
      </div>
      <div
        className={
          isSticky
            ? styles.hamburgerIconStickyContainer
            : styles.hamburgerIconContainer
        }
      >
        <GiHamburgerMenu
          onClick={() => showSettings()}
          className={styles.hamburgerIcon}
          color="white"
        />
      </div>
    </header>
  )
}

function ScrollLink({ to, text, isHidden, showSettings }) {
  return (
    <h3 className={styles.linkContainer}>
      <ReactScrollLink
        to={to}
        containerId={Constants.containerElement}
        className={isHidden ? { display: "none" } : styles.link}
        spy={true}
        smooth={true}
        hashSpy={true}
        offset={-90}
        isDynamic={true}
        onClick={() => isHidden && showSettings()}
      >
        {text}
      </ReactScrollLink>
    </h3>
  )
}

Header.prototype = {
  headerName: PropTypes.string,
}

export default Header
