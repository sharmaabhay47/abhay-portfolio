/* eslint-disable react/no-unescaped-entities */
import React from "react"

import ImageConstant from "utils/imageConstant"

import styles from "./landing.module.scss"

function Landing() {
  return (
    <div className={styles.container}>
      <div className={styles.landingBackground}>
        <img
          src={ImageConstant.CODE_BACKGROUND}
          className={styles.imageStyle}
          alt="landing"
        />
      </div>

      <div className={styles.textWrapper}>
        <div className={styles.landingTextStyle}>
          <h1>Hello, I'm Abhay Sharma</h1>
        </div>
        <h1 className={styles.landingTextStyle}>- I'm a Software Developer</h1>
      </div>
    </div>
  )
}

export default Landing
