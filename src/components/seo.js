import React from "react"
import PropTypes from "prop-types"
import { Helmet } from "react-helmet"
import { useLocation } from "@reach/router"
import { useStaticQuery, graphql } from "gatsby"

const SEO = ({ title, description, image, article }) => {
  const { pathname } = useLocation()
  const { site } = useStaticQuery(query)

  const {
    defaultTitle,
    titleTemplate,
    defaultDescription,
    siteUrl,
    defaultImage,
    linkedinUsername,
    socialLinks,
    twitterUsername,
  } = site.siteMetadata

  const seo = {
    title: title || defaultTitle,
    description: description || defaultDescription,
    image: `${siteUrl}${image || defaultImage}`,
    url: `${siteUrl}${pathname}`,
  }

  const schemaOrgWebPage = {
    "@context": "http://schema.org",
    "@type": "WebPage",
    url: siteUrl,
    headline: "Software Developer",
    inLanguage: "en",
    mainEntityOfPage: siteUrl,
    description: description || defaultDescription,
    name: "Abhay Sharma - Full Stack Developer",
    author: {
      "@id": "abhay sharma",
    },
    copyrightHolder: {
      "@id": "abhay sharma",
    },
    copyrightYear: "2020",
    creator: {
      "@id": "abhay sharma",
    },
    publisher: {
      "@id": "abhay sharma",
    },
    datePublished: "2020-06-01",
    image: {
      "@type": "ImageObject",
      url: `${siteUrl}${image || defaultImage}`,
    },
  }

  const orgaCreator = {
    "@context": "http://schema.org",
    "@id": `${siteUrl}`,
    "@type": "WebPage",
    address: {
      "@type": "PostalAddress",
      addressCountry: "India",
      addressLocality: "Pune",
    },
    name: "Abhay Sharma - Full Stack Developer",
    alternateName: "Monu",
    description: description || defaultDescription,
    url: siteUrl,
    email: "sharmaabhay47@gmail.com",
    founder: "Abhay Sharma",
    foundingDate: "2020-06-01",
    foundingLocation: "India",
    image: {
      "@type": "ImageObject",
      url: `${siteUrl}${image || defaultImage}`,
      height: "512",
      width: "512",
    },
    logo: {
      "@type": "ImageObject",
      url: `${siteUrl}${image || defaultImage}`,
      height: "60",
      width: "60",
    },
    sameAs: [
      "https://www.facebook.com/abhay.sharma.1650/",
      "https://stackoverflow.com/users/9463179/abhay-sharma",
      "https://github.com/sharmaabhay18",
      "https://www.instagram.com/abhaaayyy04/",
      "https://twitter.com/sharmaabhay_18",
      "https://www.linkedin.com/in/abhay-sharma-3955a73b/",
    ],
  }

  return (
    <Helmet title={seo.title} titleTemplate={titleTemplate}>
      <meta name="description" content={seo.description} />
      <meta name="image" content={seo.image} />
      <html lang="en" />
      {/* OpenGraph Tag*/}
      {seo.url && <meta property="og:url" content={seo.url} />}
      {<meta property="og:type" content="website" />}
      {seo.title && <meta property="og:title" content={seo.title} />}
      {seo.description && (
        <meta property="og:description" content={seo.description} />
      )}
      {seo.image && <meta property="og:image" content={seo.image} />}

      {socialLinks && <meta name="socialLinks:creator" content={socialLinks} />}

      {/* LinkenId Card*/}
      {<meta property="og:type" content="website" />}
      <meta name="linkedin:card" content="summary_large_image" />
      {linkedinUsername && (
        <meta name="linkedin:creator" content={linkedinUsername} />
      )}

      {seo.title && <meta name="linkedin:title" content={seo.title} />}
      {seo.description && (
        <meta name="linkedin:description" content={seo.description} />
      )}
      {seo.image && <meta name="linkedin:image" content={seo.image} />}

      {/* Twitter Card*/}
      {<meta property="og:type" content="website" />}
      <meta name="twitter:card" content="summary_large_image" />
      {twitterUsername && (
        <meta name="twitter:creator" content={twitterUsername} />
      )}
      {seo.url && <meta name="twitter:site" content={seo.url} />}
      {seo.title && <meta name="twitter:title" content={seo.title} />}
      {seo.description && (
        <meta name="twitter:description" content={seo.description} />
      )}
      {seo.image && <meta name="twitter:image" content={seo.image} />}

      <meta
        property="og:see_also"
        content="https://www.facebook.com/abhay.sharma.1650/"
      />
      <meta
        property="og:see_also"
        content="https://stackoverflow.com/users/9463179/abhay-sharma"
      />
      <meta property="og:see_also" content="https://github.com/sharmaabhay18" />
      <meta
        property="og:see_also"
        content="https://www.instagram.com/abhaaayyy04/"
      />
      <meta
        property="og:see_also"
        content="https://twitter.com/sharmaabhay_18"
      />
      <meta
        property="og:see_also"
        content="https://www.linkedin.com/in/abhay-sharma-3955a73b/"
      />

      <script type="application/ld+json">
        {JSON.stringify(schemaOrgWebPage)}
      </script>
      <script type="application/ld+json">{JSON.stringify(orgaCreator)}</script>
    </Helmet>
  )
}

const query = graphql`
  query SEO {
    site {
      siteMetadata {
        defaultTitle: title
        titleTemplate
        defaultDescription: description
        siteUrl: siteUrl
        defaultImage: image
        linkedinUsername
      }
    }
  }
`

export default SEO

SEO.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  article: PropTypes.bool,
}

SEO.defaultProps = {
  title: null,
  description: null,
  image: null,
  article: false,
}
