import React from "react"

import Landing from "components/landing"

import "./layout.scss"

function Layout({ children }) {
  return (
    <>
      <Landing />
      <div
        style={{
          paddingTop: 0,
        }}
      >
        <main>{children}</main>
      </div>
    </>
  )
}

export default Layout
