import React from "react"
import PropTypes from "prop-types"

import { Link as RLink } from "gatsby"

import styles from "./link.module.scss"

function Link({ link, to }) {
  return (
    <RLink to={to} className={styles.link}>
      {link}
    </RLink>
  )
}

export default Link

Link.prototype = {
  to: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
}
