import React from "react"
import PropTypes from "prop-types"

import styles from "./button.module.scss"

function Button({ buttonText, buttonStyle, link }) {
  return (
    <a className={buttonStyle ? buttonStyle : styles.buttonStyle} href={link}>
      {buttonText}
    </a>
  )
}

export default Button

Button.prototype = {
  buttonText: PropTypes.string.isRequired,
  link: PropTypes.string,
  buttonStyle: PropTypes.object,
}
