import React from "react"
import PropTypes from "prop-types"

import styles from "./card.module.scss"

function Card({ icon, cardHeader, cardContent }) {
  return (
    <div className={styles.cardContainer}>
      <div className={styles.contentContainer}>
        <img src={icon} alt="Mobile Icon" className={styles.iconStyle} />

        <h3 className={styles.iconHeaderStyle}>{cardHeader}</h3>
        <p>{cardContent}</p>
      </div>
    </div>
  )
}

export default Card

Card.prototype = {
  icon: PropTypes.string.isRequired,
  cardHeader: PropTypes.string.isRequired,
  cardContent: PropTypes.string.isRequired,
}
