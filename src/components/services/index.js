import React from "react"

import Card from "components/card"

import imageConstant from "utils/imageConstant"

import styles from "./services.module.scss"

function MyServices() {
  return (
    <div className={styles.alignCenter}>
      <div className={styles.verticalLineContainerStyle}>
        <div className={styles.verticalLineStyle} />
      </div>
      <h6 className={styles.servicesHeaderTextStyle}>SERVICE PROVIDED</h6>
      <h1 className={styles.servicesTextStyle}>Build Products From Scratch</h1>
      <div className={styles.servicesContainer}>
        <Card
          icon={imageConstant.COMPUTER_ICON}
          cardHeader="Web Design"
          cardContent="I like to code and develop things from the beginning, experienced in ReactJs, Gatsby, CSS, Sass, and Redux."
        />
        <Card
          icon={imageConstant.MOBILE_ICON}
          cardHeader="Mobile App"
          cardContent="Having experience with React Native and Flutter, I build cross platform products for both Android and iOS."
        />
      </div>
    </div>
  )
}

export default MyServices
