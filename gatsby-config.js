// eslint-disable-next-line no-undef
module.exports = {
  siteMetadata: {
    title: "Abhay Sharma - Software Developer / Full Stack Developer",
    author: "Abhay Sharma",
    titleTemplate: "Hello 🙏, I'm Abhay Sharma - Software Developer",
    description:
      "A Software Engineer 👨‍💻 by profession. I am based in Pune, India you will find me playing video games 🎮 during weekends, or get high on caffeine ☕.",
    siteUrl: "https://www.abhaysharma.dev",
    image: "/me.jpg",
    linkedinUsername: "abhay-sharma-3955a73b",
    twitterUsername: "sharmaabhay_18",
    socialLinks: {
      facebook: "https://www.facebook.com/abhay.sharma.1650/",
      stackOverflow: "https://stackoverflow.com/users/9463179/abhay-sharma",
      github: "https://github.com/sharmaabhay18",
      instagram: "https://www.instagram.com/abhaaayyy04/",
      email: "sharmaabhay47@gmail.com",
      phone: "+919028440832",
      address: "Pune, India",
    },
    keywords: [
      "React",
      "React Native",
      "Flutter",
      "Android",
      "iOS",
      "NodeJS",
      "GraphQL",
      "AWS",
      "GCP",
      "Gatsby",
      "Software developer",
      "Full Stack Developer",
      "Mobile Developer",
      "Cross Platform Mobile Developer",
    ],
    organization: {
      name: "Coditas",
      url: "https://www.abhaysharma.dev",
    },
  },
  plugins: [
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-s3`,
      options: {
        bucketName: "www.abhaysharma.dev",
        protocol: "https",
        hostname: "www.abhaysharma.dev",
        acl: null,
      },
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: "https://www.abhaysharma.dev",
        policy: [{ userAgent: "*", allow: "/" }],
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-167770497-1",
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        exclude: [`/home`],
      },
    },
    {
      resolve: `gatsby-alias-imports`,
      options: {
        aliases: {
          components: `src/components`,
          pages: `src/pages`,
          assest: `src/assest`,
          utils: `src/utils`,
        },
        extensions: ["js"],
      },
    },
  ],
}
